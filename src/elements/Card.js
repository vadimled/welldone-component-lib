import React from 'react';
import {Box} from 'grid-styled';
import styled, {css} from 'styled-components';

const Root = styled(Box)`
  padding: 15px;
  min-height: 100px;
  ${({height}) => (height
    ? css`
          height: ${height}px;
        `
    : '')};
`;

const Container = styled.div`
  width: 100%;
  border-radius: 4px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), 0 0 0 1px rgba(63, 63, 68, 0.1);
  background-color: #fff;
`;

const Header = styled.header`
  padding: 15px 15px 0;
`;

const Content = styled.div`
  padding: 15px;
  overflow: auto;
  ${({contentHeight}) => (contentHeight
    ? css`
          height: ${contentHeight}px;
        `
    : '')};
`;

const Title = styled.h4`
  font-size: 22px;
  font-weight: 300;
  line-height: 30px;
  color: #333;
  margin: 0;
`;

const SubTitle = styled.p`
  font-size: 14px;
  font-weight: 400;
  color: #444;
  margin-bottom: 0;
`;

const Card = ({title, subTitle, contentHeight, height, children, props}) => (
  <Root {...{width: 1, height, props}}>
    <Container>
      {(title || subTitle) && (
        <Header>
          {title && (
          <Title>
            {title}
          </Title>
          )}
          {subTitle && (
          <SubTitle>
            {subTitle}
          </SubTitle>
          )}
        </Header>
      )}
      <Content contentHeight={contentHeight}>
        {children}
      </Content>
    </Container>
  </Root>
);

export default Card;
