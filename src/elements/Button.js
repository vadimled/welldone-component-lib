import styled from 'styled-components';
import {darken} from 'polished';

const Button = styled.button.attrs({
  children: props => props.label,
})`
  transition: all 60ms ease-in;
  font-weight: 400;
  padding: 8px 16px;
  color: #444;
  display: inline-block;
  margin-bottom: 0;
  font-size: 14px;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  touch-action: manipulation;
  cursor: pointer;
  user-select: none;
  background: transparent none;
  border: 1px solid transparent;
  border-radius: 4px;
  ${({primary}) => primary && `
    color: #ffffff;
    background-color:#23CCEF;
    opacity: 1;
    &:hover {
      background-color: ${darken(0.06, '#23CCEF')};
      color: #FFFFFF;   
    }
  `} 
  ${({basic}) => basic && `
    color: #444444;
    background-color: #ffffff;
    border: 1px solid #CCCCCC;
    opacity: 1;
    &:hover {
      background-color: #DDDDDD;
    }
  `} 
  ${({disabled}) => disabled && `
    opacity: 0.5; 
    cursor: not-allowed;
  `} 
  
  ${({small}) => small && `
    padding: 4px 8px;    
    font-size: 12px;
  `}   
`;

export default Button;
